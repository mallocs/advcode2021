import getInput from "../getInput";

const DOT_RE = /(\d+),(\d+)/;
const FOLD_RE = /fold along (x|y)=(\d+)/;

type Op = {
  axis: string;
  value: number;
};

type Dot = {
  x: number;
  y: number;
};

const parseInput = (input: string): any => {
  const dots: Dot[] = [];
  const ops: Op[] = [];
  input.split("\n").forEach((entry) => {
    const dotMatch = DOT_RE.exec(entry);
    if (dotMatch) {
      dots.push({ x: Number(dotMatch[1]), y: Number(dotMatch[2]) });
    }
    const foldMatch = FOLD_RE.exec(entry);
    if (foldMatch) {
      ops.push({ axis: foldMatch[1], value: Number(foldMatch[2]) });
    }
  });

  return {
    dots: dots,
    ops: ops,
  };
};

const foldAlongY = (dot: Dot, axis: number) =>
  dot.y <= axis ? dot : { ...dot, y: axis * 2 - dot.y };

const foldAlongX = (dot: Dot, axis: number) =>
  dot.x <= axis ? dot : { ...dot, x: axis * 2 - dot.x };

const fold = (dots: Dot[], op: Op) =>
  dots.map((dot: Dot) =>
    op.axis === "y" ? foldAlongY(dot, op.value) : foldAlongX(dot, op.value)
  );

const uniqifyDots = (dots: Dot[]): Dot[] =>
  dots.filter(
    (v, i, a) => a.findIndex((t: Dot) => t.x === v.x && t.y === v.y) === i
  );

const printDots = (dots: Dot[]) => {
  let maxY = 0,
    maxX = 0;
  dots.forEach((dot) => {
    if (dot.x > maxX) {
      maxX = dot.x;
    }
    if (dot.y > maxY) {
      maxY = dot.y;
    }
  });
  const grid = Array.from({ length: maxY + 1 }, () =>
    Array.from({ length: maxX + 1 }, () => ".")
  );
  dots.forEach((dot) => {
    grid[dot.y][dot.x] = "#";
  });
  grid.forEach((row) => console.log(`${row.join()}\n`));
};

const partOne = ({ dots, ops }: { dots: Dot[]; ops: Op[] }): number =>
  uniqifyDots(fold(dots, ops[0])).length;

const partTwo = ({ dots, ops }: { dots: Dot[]; ops: Op[] }) => {
  ops.forEach((op: Op) => (dots = fold(dots, op)));
  printDots(uniqifyDots(dots));
};

console.log(`Part one: ${partOne(parseInput(getInput()))}`); // 712
console.log(`Part two: `);
partTwo(parseInput(getInput())); // BLHFJPJF
