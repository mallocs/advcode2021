import getInput from '../getInput'

const parseInput = (input : string) => input.split("\n")

const isZeroMostCommonAtPosition = (input : Array<string>, i : number) : boolean => {
    let zeroCount = 0
    let oneCount = 0
    input.forEach((item : string) =>
        item[i] === "0" ? zeroCount++ : oneCount++
    )
    return zeroCount > oneCount
}
const partOne = (input: Array<string>) : number => {
    let gammaStr = ""
    let epsilonStr = ""

    for (let i = 0; i < input[0].length; i++) {
        if(isZeroMostCommonAtPosition(input, i)) {
            gammaStr += "0"
            epsilonStr += "1"
        } else {
            gammaStr += "1"
            epsilonStr += "0"
        }
    }

    return parseInt(gammaStr, 2) * parseInt(epsilonStr, 2)
}

const findRating = (input: Array<string>, commonBit: string, uncommonBit: string) : number | undefined => {
    for (let i = 0; i < input[0].length; i++) {
        const bit = isZeroMostCommonAtPosition(input, i) ? commonBit : uncommonBit
        input = input.filter(item => item[i] === bit)
        if (input.length === 1) {
            return parseInt(input[0], 2)
        }
    }
}

const partTwo = (input: Array<string>) : number =>
    findRating(input.slice(), "0", "1")!
    * findRating(input.slice(), "1", "0")!

const inputArray = parseInput(getInput())
console.log(`Part one: ${partOne(inputArray)}`) // 4138664
console.log(`Part two: ${partTwo(inputArray)}`) // 4273224