"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var getInput_1 = require("../getInput");
var parseInput = function (input) { return input.split("\n"); };
var isZeroMostCommonAtPosition = function (input, i) {
    var zeroCount = 0;
    var oneCount = 0;
    input.forEach(function (item) {
        return item[i] === "0" ? zeroCount++ : oneCount++;
    });
    return zeroCount > oneCount;
};
var partOne = function (input) {
    var gammaStr = "";
    var epsilonStr = "";
    for (var i = 0; i < input[0].length; i++) {
        if (isZeroMostCommonAtPosition(input, i)) {
            gammaStr += "0";
            epsilonStr += "1";
        }
        else {
            gammaStr += "1";
            epsilonStr += "0";
        }
    }
    return parseInt(gammaStr, 2) * parseInt(epsilonStr, 2);
};
var findRating = function (input, commonBit, uncommonBit) {
    var _loop_1 = function (i) {
        var bit = isZeroMostCommonAtPosition(input, i) ? commonBit : uncommonBit;
        input = input.filter(function (item) { return item[i] === bit; });
        if (input.length === 1) {
            return { value: parseInt(input[0], 2) };
        }
    };
    for (var i = 0; i < input[0].length; i++) {
        var state_1 = _loop_1(i);
        if (typeof state_1 === "object")
            return state_1.value;
    }
};
var partTwo = function (input) {
    return findRating(input.slice(), "0", "1")
        * findRating(input.slice(), "1", "0");
};
var inputArray = parseInput((0, getInput_1.default)());
console.log("".concat(partOne(inputArray))); // 4138664
console.log("".concat(partTwo(inputArray))); // 4273224
//# sourceMappingURL=index.js.map