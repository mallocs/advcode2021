import getInput from '../getInput'

const parseInput = (input : string) : number[] => input.split(",").map(Number)

function findMedian(numbers: number[]) {
    const sorted = numbers.slice().sort((a, b) => a - b);
    const middle = Math.floor(sorted.length / 2);

    if (sorted.length % 2 === 0) {
        return (sorted[middle - 1] + sorted[middle]) / 2;
    }

    return sorted[middle];
}
const computeAverage = (array: number[]) => array.reduce((a, b) => a + b) / array.length;
const partOne = (input : number[]) : number => {
    const median = findMedian(input)
    return input.reduce((acc, previous) => {
        acc += Math.abs(previous - median)
        return acc
    }, 0)
}

const computeFuel = (average: number, positions: number[]) => positions.reduce(
    (acc, previous) => {
        const n = Math.abs(previous - average)
        return acc += ((n+1)*(n))/2
    }, 0)

const partTwo = (input : Array<number>) : number =>
    Math.min(computeFuel(Math.floor(computeAverage(input)), input), computeFuel(Math.ceil(computeAverage(input)), input))

const inputArray = parseInput(getInput())

console.log(`Part one: ${partOne(inputArray)}`) // 355592
console.log(`Part two: ${partTwo(inputArray)}`) // 101618069