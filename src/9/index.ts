import getInput from '../getInput'

interface Position {
    row: number,
    col: number
}

class Grid {
    grid: number[][]
    _basinSizes: number[]

    constructor(input: string) {
        this.grid = input.split("\n").reduce(
            (acc : number[][], current, currentIndex) => {
                acc[currentIndex] = current.split("").map(Number)
                return acc
            }, [])
    }

    columns() : number {
        return this.grid[0].length
    }

    rows() : number {
        return this.grid.length
    }

    valueAtPosition(position: Position) : number {
        return this.grid[position.row][position.col]
    }

    neighborPositionsByPosition(position: Position) : Position[] {
        return this.neighborPositions(position.row, position.col)
    }

    neighborPositions(row : number, col : number) : Position[] {
        return [
            ...(col - 1 >= 0) ? [{row, col: col - 1}] : [],
            ...(row - 1 >= 0) ? [{row: row - 1, col}] : [],
            ...(col + 1 < this.columns()) ? [{row, col: col + 1}] : [],
            ...(row + 1 < this.rows()) ? [{row: row + 1, col}] : []
        ]
    }

    neighborValues(row : number, col : number) {
        this.neighborPositions(row, col).map(this.valueAtPosition)
    }

    lowPoints() : Position[] {
        return this.grid.reduce(
            (acc : Position[], current : number[], row : number) : Position[] =>
                [...acc, ...current.reduce(
                    (innerAcc : Position[], valueAtPosition : number, col : number) : Position[] =>
                        this.neighborPositions(row, col).every(
                            neighbor => this.valueAtPosition(neighbor) > valueAtPosition
                        ) ? [...innerAcc, {row, col}] : [...innerAcc], []
                )], []
        )
    }

    riskValueAtPosition(position: Position) : number {
        return 1 + this.valueAtPosition(position)
    }

    totalRisk() : number {
        return this.lowPoints().reduce((acc, current) => acc + this.riskValueAtPosition(current), 0)
    }

    _expandBasin(position: Position) : number {
        if (this.valueAtPosition(position) === 9) {
            return 0
        }
        this.grid[position.row][position.col] = 9
        return 1 + this.neighborPositionsByPosition(position).reduce((acc, currentPosition) => acc + this._expandBasin(currentPosition), 0)
    }

    basinSizes() : number[] {
        if (this._basinSizes === undefined) {
            this._basinSizes = this.lowPoints().reduce((acc: number[], position) => [...acc, this._expandBasin(position)], [])
        }
        return this._basinSizes
    }

    threeLargestBasinsSizeProduct() : number {
        return this.basinSizes().sort((a, b) => b - a).slice(0,3).reduce((acc, current) => acc * current)
    }
}

const partOne = (input : string) : number => new Grid(input).totalRisk()
const partTwo = (input : string) : number => new Grid(input).threeLargestBasinsSizeProduct()

console.log(`Part one: ${partOne(getInput())}`) // 580
console.log(`Part two: ${partTwo(getInput())}`) // 856716

