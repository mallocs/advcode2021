import getInput from '../getInput'

const OBSERVATION_RE = /([a-z]+ ){10}|([a-z]+ ){4}\n/g

const partOne = (input : string) : any =>
    input.split("\n").reduce((acc, current) => {
                return acc + current.split("|")[1].split(" ").reduce((lineAcc, lineCurrent) => {
                    if (lineCurrent.length === 2 ||lineCurrent.length === 4 ||lineCurrent.length === 3 ||lineCurrent.length === 7){
                        return lineAcc + 1
                    } else {
                        return lineAcc
                    }
                }, 0)

    }, 0)

const numbersToSegmentCount = {0: 6, 1: 2, 2: 5, 3: 5, 4: 4, 5: 5, 6: 6, 7: 3, 8: 7, 9: 6}
const segmentChrs = ['a', 'b', 'c', 'd', 'e', 'f', 'g']
const easy = [1, 4, 7, 8]

const findCode = (input : string[]) => {
    const segments = {}

    // easy ones
    const digits = input.reduce((acc : object, codeEntry : string) => {
        const digit = easy.filter(num => numbersToSegmentCount[num] === codeEntry.length)[0]
        if (digit) {
            acc[digit] = codeEntry
        }
        return acc
    }, [])

    // a is the segment in 7 that's not in 1
    segments['a'] = digits[7].split('').filter(chr => !digits[1].includes(chr))[0]

    // 6 => doesn't contain both chars from 1
    digits[6] = input.filter(codeEntry => codeEntry.length === 6 && !digits[1].split('').every(chr => codeEntry.includes(chr)))[0]

    // c is segment not in 6
    segments['c'] = segmentChrs.filter(chr => !digits[6].split('').includes(chr))[0]

    // f is segment in 1 that isn't c
    segments['f'] = digits[1].split('').filter(chr => chr !== segments['c'])[0]

    // 9 => contains everything from 4
    digits[9] = input.filter(codeEntry => codeEntry.length === 6 && digits[4].split("").filter(chr => !codeEntry.includes(chr)).length === 0)[0]

    // 0 => last of 6 char chars
    digits[0] = input.filter(codeEntry => codeEntry.length === 6 && codeEntry !== digits[6] && codeEntry !== digits[9])[0]

    // 2 = 5 segments but not including f
    digits[2] = input.filter(codeEntry => codeEntry.length === 5 && !codeEntry.includes(segments['f']))[0]

    // 5 = missing one char from 6
    digits[5] = input.filter(codeEntry => codeEntry.length === 5 && digits[6].split("").filter(chr => !codeEntry.includes(chr)).length === 1)[0]

    // 3 => last of 5 char chars
    digits[3] = input.filter(codeEntry => codeEntry.length === 5 && codeEntry !== digits[5] && codeEntry !== digits[2])[0]

    return digits
}

const decode = (str, code) => Number(str.split(" ").reduce((acc, current) => {
        const codeKey = code.filter(codeEntry => codeEntry.length === current.length &&
            codeEntry.split("").filter(chr => !current.includes(chr)).length === 0)[0]
        acc += code.indexOf(codeKey)
        return acc
    },""))

const partTwo = (input : string) : number => {
    return input.split("\n").reduce((acc, current) => {
        let code = findCode(current.split(" | ")[0].split(" "))
        let num = decode(current.split(" | ")[1], code)
        return acc + num
    }, 0)
}

console.log(`Part one: ${partOne(getInput())}`) // 381
console.log(`Part two: ${partTwo(getInput())}`) // 1023686

