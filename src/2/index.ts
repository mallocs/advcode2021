import getInput from '../getInput'

type Axis = "horizontal" | "depth"
interface Movement {
    axis: Axis,
    units: number
}
const parseInput = (input : string) : Array<Movement> => input.split("\n").map(rawLine => {
    let [axis, units] = rawLine.split(" ")
    return {
        axis: axis === "forward" ? "horizontal" : "depth",
        units: axis === "up" ? Number(units) * -1 : Number(units)
    }
})

const partOne = (input: Array<Movement>) : number => input.filter(item => item.axis === "depth").reduce((previous, current) => current.units + previous, 0) *
        input.filter(item => item.axis === "horizontal").reduce((previous, current) => current.units + previous, 0)

const partTwo = (input: Array<Movement>) : number => {
    let aim = 0
    return input.filter(item => item.axis === "horizontal").reduce((previous, current) => current.units + previous, 0) *
        input.reduce((previous, current) => {
            if (current.axis === "depth") {
                aim += current.units
                return previous
            }
            return previous + current.units * aim
        }, 0)
}

const inputArray = parseInput(getInput())
console.log(`Part one: ${partOne(inputArray)}`)
console.log(`Part two: ${partTwo(inputArray)}`)
