import getInput from '../getInput'

type BoardRow = Array<number|"x">
type Board = Array<BoardRow>

const parseInput = (input : string) => input.split("\n")
const parseNumbers = (input: string) => input.split(",").map(Number)
const parseBoards = (input: Array<string>) => {
    const DIGITS_RE = /\d+/g
    let boards = []
    let currentBoard = []
    for (let i = 0; i < input.length; i++) {
        if (i%6 === 5) {
            boards.push(currentBoard)
            currentBoard = []
        } else {
            currentBoard.push((input[i].match(DIGITS_RE) || []).map(Number))
        }
    }
    return boards
}

const markBoard = (board: Board, number : number) =>
    board.map(row =>  row.map(item => item === number ? "x" : item))

const checkBoardColumn = (board: Board, column: number) => board.every(row => row[column] !== "x" ? false : true)

const checkBoardRow = (board: Board, row: number) => board[row].toString() === 'x,x,x,x,x'

const checkBoard = (board: Board) => {
    for (let i = 0; i < 5; i++) {
        if (checkBoardRow(board, i) || checkBoardColumn(board, i)) {
            return true
        }
    }
}

const sumBoard = (board: Board) =>
    board.reduce(
        (previous: number, current : BoardRow) =>
            previous + current.filter(
                item => typeof item === "number"
            ).reduce(
                (previous, current) => previous + current, 0
            ), 0
    )

const partOne = (numbers : Array<number>, boards : Array<Board>) : number => {
    for (let number of numbers) {
        for (let i = 0; i < boards.length; i++) {
            boards[i] = markBoard(boards[i], number)
            if (checkBoard(boards[i])) {
                return number * sumBoard(boards[i])
            }
        }
    }
    return -1
}

const partTwo = (numbers : Array<number>, boards : Array<Board>) : number => {
    let lastBoard
    for (let number of numbers) {
        lastBoard = boards[0]
        boards = boards.map(board => markBoard(board, number)).filter(board => !checkBoard(board))
        if (boards.length === 0) {
            return number * (sumBoard(lastBoard) - number)
        }
    }
    return -1
}

const inputArray = parseInput(getInput())
const numbers = parseNumbers(inputArray[0])
const boards = parseBoards(inputArray.slice(2))

console.log(`${partOne(numbers, boards)}`) // 2496
console.log(`${partTwo(numbers, boards)}`) // 25925