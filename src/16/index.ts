import getInput from "../getInput";

const parseInput = (input: string): string =>
  input
    .split("")
    .map((chr) => parseInt(chr, 16).toString(2).padStart(4, "0"))
    .join("");

const parseTypeId = (input: string): number => parseInt(input, 2);
const parseVersion = (input: string): number => parseInt(input, 2);

const parseHeader = (input: string): number[] => [
  parseVersion(input.slice(0, 3)),
  parseTypeId(input.slice(3, 6)),
];

// return value and start index for next packet
const parseLiteralValue = (input: string, index: number): number[] => {
  let bitString = "";
  while (index <= input.length) {
    const keepGoing = input.charAt(index);
    const bits = input.slice(index + 1, index + 5);
    bitString += bits;
    index += 5;
    if (keepGoing === "0") return [parseInt(bitString, 2), index];
  }
  return [-1, -1];
};

const parseOperatorTotalBits = (input: string, index: number): number[] => {
  const totalBits = parseInt(input.slice(index, index + 15), 2);
  index += 15;
  let total = 0;
  let startIndex = index;
  while (index - startIndex < totalBits) {
    const parsed = parsePacket(input, index);
    total += parsed[0];
    index = parsed[1];
  }

  return [total, index];
};

const parseOperatorSubpacketParts = (
  input: string,
  index: number
): number[] => {
  const totalPackets = parseInt(input.slice(index, index + 11), 2);
  index += 11;
  let count = 0;
  let total = 0;
  while (count < totalPackets) {
    const parsed = parsePacket(input, index);
    total += parsed[0];
    index = parsed[1];
    count++;
  }

  return [total, index];
};

const parseOperatorValue = (input: string, index: number): number[] =>
  input[index] === "0"
    ? parseOperatorTotalBits(input, index + 1)
    : parseOperatorSubpacketParts(input, index + 1);

let versionCount = 0; // global, didn't realize this was the goal of part one. sure to change for part 2
const parsePacket = (input: string, index: number): number[] => {
  const [version, type] = parseHeader(input.slice(index, index + 6));
  versionCount += version;
  if (type === 4) {
    return parseLiteralValue(input, index + 6);
  } else {
    return parseOperatorValue(input, index + 6);
  }
};

const partOne = (input: string): number => {
  let sum = 0;
  let index = 0;

  while (index <= input.length && parseInt(input.slice(index), 2) !== 0) {
    let parsed = parsePacket(input, index);
    sum += parsed[0];
    index = parsed[1];
  }
  return versionCount;
};

const partTwo = (input: string): number => -1;

console.log(`Part one: ${partOne(parseInput(getInput()))}`); // 917
// console.log(`Part two: ${partTwo(parseInput(getInput()))}`); //
