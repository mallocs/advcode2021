import getInput from "../getInput";

type Grid = number[][];
type Point = {
  x: number;
  y: number;
};

const parseInput = (input: string): Grid =>
  input.split("\n").reduce((accumulator, currentLine) => {
    const line = currentLine.split("").reduce((lineAccumulator, value) => {
      lineAccumulator.push(Number(value));
      return lineAccumulator;
    }, []);
    accumulator.push(line);
    return accumulator;
  }, []);

const getNeighbors = (p: Point, g: Grid, factor: number = 1): Point[] => {
  const neighbors = [];
  if (p.x - 1 >= 0) {
    neighbors.push({ x: p.x - 1, y: p.y });
  }
  if (p.x + 1 < g[0].length * factor) {
    neighbors.push({ x: p.x + 1, y: p.y });
  }
  if (p.y - 1 >= 0) {
    neighbors.push({ x: p.x, y: p.y - 1 });
  }
  if (p.y + 1 < g.length * factor) {
    neighbors.push({ x: p.x, y: p.y + 1 });
  }
  return neighbors;
};

const getGraphValue = (graph: Grid, point: Point): number => {
  const basePointValue =
    graph[point.y % graph.length][point.x % graph[0].length];
  const distance =
    Math.floor(point.x / graph[0].length) + Math.floor(point.y / graph.length);
  return ((basePointValue + distance - 1) % 9) + 1;
};

const findMinPathValue = (
  graph: Grid,
  source: Point = { x: 0, y: 0 },
  destination: Point = { x: graph[0].length - 1, y: graph.length - 1 },
  factor: number = 1
): number => {
  const queue: Point[] = [source];
  const totalRisk = Array(graph[0].length * factor)
    .fill(0)
    .map((_) => Array(graph.length * factor).fill(Number.MAX_SAFE_INTEGER));

  let current = queue.shift();
  totalRisk[current!.y][current!.x] = 0; // Don't count starting square
  while (
    current !== undefined &&
    !(current.x === destination.x && current.y === destination.y)
  ) {
    queue.sort((a, b) => totalRisk[a.y][a.x] - totalRisk[b.y][b.x]); // priority queue would be faster
    getNeighbors(current, graph, factor).forEach((neighbor) => {
      // relax neighbors in queue
      const neighborIndex = queue.findIndex(
        (queueElement) =>
          queueElement.x === neighbor.x && queueElement.y === neighbor.y
      );
      if (
        neighborIndex === -1 &&
        totalRisk[neighbor.y][neighbor.x] === Number.MAX_SAFE_INTEGER
      ) {
        queue.push(neighbor);
      }
      totalRisk[neighbor.y][neighbor.x] = Math.min(
        totalRisk[neighbor.y][neighbor.x],
        totalRisk[current!.y][current!.x] + getGraphValue(graph, neighbor)
      );
    });

    current = queue.shift();
  }

  return totalRisk[destination.y][destination.x];
};

const partOne = (input: Grid): number => findMinPathValue(input);

const partTwo = (input: Grid): number =>
  findMinPathValue(
    input,
    { x: 0, y: 0 },
    { x: 5 * input[0].length - 1, y: 5 * input.length - 1 },
    5
  );

console.log(`Part one: ${partOne(parseInput(getInput()))}`); // 604
console.log(`Part two: ${partTwo(parseInput(getInput()))}`); // 2907
