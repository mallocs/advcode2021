import getInput from "../getInput";

type Template = string;

type Rule = {
  code: string;
  insertion: string;
};

const parseInput = (input: string): { template: Template; rules: Rule[] } => ({
  template: input.split("\n")[0],
  rules: input
    .split("\n")
    .slice(2)
    .map((line) => ({
      code: line.split(" -> ")[0],
      insertion: line.split(" -> ")[1],
    })),
});

// const doStepFactory =
//   (ruleTable: Map<string, string>) => (template: Template) => {
//     let nextTemplate = [template[0]];
//     for (let i = 1; i < template.length; i++) {
//       const insertion = ruleTable.get(`${template[i - 1]}${template[i]}`);
//       if (insertion !== undefined) nextTemplate.push(insertion);
//       nextTemplate.push(template[i]);
//     }
//     return nextTemplate.join("");
//   };

// const getFrequencyMap = (str: string) =>
//   str.split("").reduce((frequencyMap, currentChar) => {
//     frequencyMap[currentChar] =
//       frequencyMap[currentChar] === undefined
//         ? 1
//         : frequencyMap[currentChar] + 1;
//     return frequencyMap;
//   }, {});

// const findDifference = ({
//   template,
//   rules,
//   steps,
// }: {
//   template: Template;
//   rules: Rule[];
//   steps: number;
// }): number => {
//   const stepFn = doStepFactory(
//     new Map(rules.map((rule) => [rule.code, rule.insertion]))
//   );
//   const finalTemplate = [...Array(steps)].reduce(
//     (accumulator, _) => stepFn(accumulator),
//     template.slice()
//   );
//   const frequencyMapValues = Object.values(getFrequencyMap(finalTemplate)).sort(
//     (a, b) => b - a
//   );

//   return (
//     frequencyMapValues[0] - frequencyMapValues[frequencyMapValues.length - 1]
//   );
// };

// const partOne = ({
//   template,
//   rules,
//   steps,
// }: {
//   template: Template;
//   rules: Rule[];
//   steps: number;
// }): number => findDifference({ template, rules, steps });

const doFastStepFactory =
  (ruleTable: Map<string, string>) => (values: Map<string, number>) => {
    const nextValues: Map<string, number> = new Map();
    for (let [key, value] of values.entries()) {
      const insertion = ruleTable.get(key);
      nextValues.set(
        `${key[0]}${insertion}`,
        (nextValues.get(`${key[0]}${insertion}`) ?? 0) + value
      );
      nextValues.set(
        `${insertion}${key[1]}`,
        (nextValues.get(`${insertion}${key[1]}`) ?? 0) + value
      );
    }
    return nextValues;
  };

const partTwo = ({
  template,
  rules,
  steps,
}: {
  template: Template;
  rules: Rule[];
  steps: number;
}): number => {
  let values: Map<string, number> = new Map();
  for (let i = 1; i < template.length; i++) {
    values.set(
      `${template[i - 1]}${template[i]}`,
      (values.get(`${template[i - 1]}${template[i]}`) ?? 0) + 1
    );
  }
  const stepFn = doFastStepFactory(
    new Map(rules.map((rule) => [rule.code, rule.insertion]))
  );
  for (let i = 0; i < steps; i++) {
    values = new Map(stepFn(values));
  }
  const finalValues: Map<string, number> = new Map();
  for (let [key, value] of values.entries()) {
    finalValues.set(key[0], (finalValues.get(key[0]) ?? 0) + value);
    finalValues.set(key[1], (finalValues.get(key[1]) ?? 0) + value);
  }

  const frequencyMapValues = Object.values(
    Array.from(finalValues.values())
  ).sort((a, b) => b - a);

  return (
    Math.ceil(frequencyMapValues[0] / 2) -
    Math.ceil(frequencyMapValues[frequencyMapValues.length - 1] / 2)
  );
};

console.log(`Part one: ${partTwo({ ...parseInput(getInput()), steps: 10 })}`); // 2549
console.log(`Part two: ${partTwo({ ...parseInput(getInput()), steps: 40 })}`); // 2516901104210
