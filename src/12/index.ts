import getInput from "../getInput";

class Graph {
  adj;

  constructor() {
    this.adj = new Map<string, string[]>();
  }

  private addDirectionalEdge(v: string, w: string) {
    if (!this.adj.get(v)) {
      this.adj.set(v, []);
    }
    this.adj.get(v)!.push(w);
  }

  addEdge(v: string, w: string) {
    this.addDirectionalEdge(v, w);
    this.addDirectionalEdge(w, v);
  }

  getPaths(start: string, end: string, revisits = 0): string[][] {
    const isVisited = new Map(Array.from(this.adj.keys(), (key) => [key, 0]));
    return this.getPathsRecursive(start, end, isVisited, [start], [], revisits);
  }

  satisfiesRevisitCriteria(
    node: string,
    isVisited: Map<string, number>,
    revisits: number
  ): boolean {
    return (
      isVisited.get(node)! <= revisits &&
      Array.from(isVisited.values()).filter(
        (value: number) => value >= revisits
      ).length === 0 &&
      node !== "start" &&
      node !== "end"
    );
  }

  private getPathsRecursive(
    current: string,
    end: string,
    isVisited: Map<string, number>,
    currentPath: string[],
    paths: string[][],
    revisits: number
  ): string[][] {
    if (current === end) {
      paths.push(currentPath.slice());
      return paths;
    }

    if (current.toLowerCase() === current) {
      isVisited.set(current, isVisited.get(current)! + 1);
    }

    for (let adjacentNode of this.adj.get(current)!) {
      if (
        isVisited.get(adjacentNode) === 0 ||
        this.satisfiesRevisitCriteria(adjacentNode, isVisited, revisits)
      ) {
        currentPath.push(adjacentNode);
        this.getPathsRecursive(
          adjacentNode,
          end,
          new Map(isVisited),
          currentPath,
          paths,
          revisits
        );
        // remove current node from path
        currentPath.pop();
      }
    }

    isVisited.set(current, 0);

    return paths;
  }

  printGraph() {
    // get all the vertices
    var get_keys = this.adj.keys();

    // iterate over the vertices
    for (var i of get_keys) {
      // great the corresponding adjacency list
      // for the vertex
      var get_values = this.adj.get(i);
      var conc = "";

      // iterate over the adjacency list
      // concatenate the values into a string
      for (var j of get_values!) conc += j + " ";

      // print the vertex and its adjacency list
      console.log(i + " -> " + conc);
    }
  }
}

const parseInput = (input: string): Graph => {
  const g = new Graph();

  input.split("\n").forEach((line: string) => {
    const [v, w] = line.split("-");
    g.addEdge(v, w);
  });

  return g;
};

const partOne = (input: Graph): number => {
  const paths = input.getPaths("start", "end");
  return paths.length;
};

const partTwo = (input: Graph): number => {
  const paths = input.getPaths("start", "end", 2);
  return paths.length;
};

console.log(`Part one: ${partOne(parseInput(getInput()))}`); // 3708
console.log(`Part two: ${partTwo(parseInput(getInput()))}`); // 93858
