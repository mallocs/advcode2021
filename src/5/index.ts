import getInput from '../getInput'

interface Point {
    x: number,
    y: number
}
interface Line {
    p1: Point,
    p2: Point
}

const LINE_RE = /(\d+),(\d+)[\s]+->[\s]+(\d+),(\d+)/
const parseInput = (input : string) : Line[] => input.split("\n").map(parseLine)

const sortPoints = (left: Point, right: Point) => (left.x - right.x) || (left.y - right.y)

const parseLine = (input : string) : Line => {
    let match = (input.trim().match(LINE_RE) || []).map(Number)
    const line = [{x: match[1], y: match[2]},  {x: match[3], y: match[4]}].sort(sortPoints)
    return {p1: line[0], p2: line[1]}
}

const getGrid = (size: number) => {
    let grid = [];
    for (var i = 0; i < size; i++) {
        grid[i] = new Array(size).fill(0);
    }
    return grid
}

const run = (input: Line[], gridSize: number, includeDiagonals = false) : number => {
    const grid = getGrid(gridSize)
    for (const { p1, p2 } of input) {
        if (p1.y === p2.y) {
            for (let i = p1.x; i <= p2.x; i++) {
                grid[p1.y][i] += 1
            }
        } else if (p1.x === p2.x) {
            for (let i = p1.y; i <= p2.y; i++) {
                grid[i][p1.x] += 1
            }
        } else if (includeDiagonals) {
            if (p1.y > p2.y) {
                for (let xi = p1.x, yi = p1.y; xi <= p2.x; xi++, yi--) {
                    grid[yi][xi] += 1
                }
            } else {
                for (let xi = p1.x, yi = p1.y; xi <= p2.x; xi++, yi++) {
                    grid[yi][xi] += 1
                }
            }
        }
    }
    return grid.reduce((previous, current) => {
        return previous + current.reduce((innerPrevious, innerCurrent) => innerCurrent >= 2 ? innerPrevious + 1 : innerPrevious, 0)
    }, 0)
}

const input = parseInput(getInput())
console.log(`Part one: ${run(input, 1000, false)}`) // 7297
console.log(`Part two: ${run(input, 1000, true)}`) // 21038