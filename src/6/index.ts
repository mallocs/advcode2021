import getInput from '../getInput'

const parseInput = (input : string) : number[] => input.split(",").map(Number)

const cycleCounts = (input : number[]) =>
    input.reduce((previous, current) => (previous[current] += 1) && previous, Array(9).fill(0))

const runCycle = (input : number[]) => {
    let nextCycle = Array(9)
    for (let i = 8; i >= 1; i--) {
        nextCycle[i - 1] = input[i]
    }
    nextCycle[6] += input[0]
    nextCycle[8] = input[0]
    return nextCycle
}

const run = (input: number[], days: number) : number => {
    let day = 0
    let counts = cycleCounts(input.slice())
    while (day < days) {
        counts = runCycle(counts)
        day++
    }
    return Object.values(counts).reduce((previous, current) => previous + current, 0)
}

const inputArray = parseInput(getInput())

console.log(`Part one: ${run(inputArray, 80)}`)
console.log(`Part two: ${run(inputArray, 256)}`)