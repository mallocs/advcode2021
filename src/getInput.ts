import { readFileSync } from 'fs'
import { resolve, dirname } from 'path'

export default (filename='input.txt'): string => {
    return readFileSync(
        resolve(dirname(require.main!.filename), filename),
        'utf8',
    ).trim()
}