import getInput from "../getInput";

type Range = { x: number[]; y: number[] };
type Position = { x: number; y: number };
const TARGET_RE = /target area: x=(\d+)..(\d+), y=(-?\d+)..(-?\d+)/;

const parseInput = (input: string): Range => {
  let match = input.match(TARGET_RE);
  return {
    x: [Number(match![1]), Number(match![2])],
    y: [Number(match![3]), Number(match![4])],
  };
};

const isInTarget = (position: Position, target: Range): boolean =>
  position.x >= target.x[0] &&
  position.x <= target.x[1] &&
  position.y >= target.y[0] &&
  position.y <= target.y[1];

const hasMissedTarget = (
  position: Position,
  target: Range,
  velocity: Position
): boolean => position.y < target.y[0] && velocity.x === 0;

// const isOvershoot = (position: Position, target: Range): boolean =>
//   position.x >= target.x[1] && position.y <= target.y[0];

const doStep = (
  position: Position,
  velocity: Position
): { position: Position; velocity: Position } => {
  let xVelocity = 0;
  if (velocity.x > 0) {
    xVelocity = velocity.x - 1;
  } else if (velocity.x < 0) {
    xVelocity = velocity.x + 1;
  }
  return {
    position: { x: position.x + velocity.x, y: position.y + velocity.y },
    velocity: { x: xVelocity, y: velocity.y - 1 },
  };
};

const shoot = (
  velocity: Position,
  target: Range
): { maxY: number; isInTarget: boolean } => {
  let position = { x: 0, y: 0 };
  let maxY = 0;
  while (!hasMissedTarget(position, target, velocity)) {
    maxY = position.y > maxY ? position.y : maxY;
    if (isInTarget(position, target)) {
      return { maxY, isInTarget: true };
    }
    const next = doStep(position, velocity);
    position = next.position;
    velocity = next.velocity;
  }
  return {
    maxY: -1,
    isInTarget: false,
  };
};

const run = (input: Range): number[] => {
  let maxY = 0;
  let inTargetVelocities = 0;
  for (let xVal = 1; xVal <= input.x[1]; xVal++) {
    for (let yVal = input.y[0]; yVal <= 1000; yVal++) {
      let { maxY: currentMaxY, isInTarget } = shoot(
        { x: xVal, y: yVal },
        input
      );

      if (isInTarget) inTargetVelocities++;
      if (currentMaxY > maxY) {
        maxY = currentMaxY;
      }
    }
  }

  return [maxY, inTargetVelocities];
};

const partOne = (input: Range): number => run(input)[0];
const partTwo = (input: Range): number => run(input)[1];

console.log(`Part one: ${partOne(parseInput(getInput()))}`); // 15400
console.log(`Part two: ${partTwo(parseInput(getInput()))}`); // 5844
