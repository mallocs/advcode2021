import { isArray } from "util";
import getInput from "../getInput";

type ValueOrArray<T> = T | ValueOrArray<T>[];
type Tuple = [number, number];
type SnailfishNumber = ValueOrArray<Tuple> | 0 | Tuple;

const parseInput = (input: string): SnailfishNumber[] =>
  input.split("\n").map((line) => JSON.parse(line));

const NUM_RE = /(\d+)/dg;
const explodeAsString = (
  snailNum: SnailfishNumber
): [SnailfishNumber, boolean] => {
  let snailNumString = JSON.stringify(snailNum);
  for (let i = 0, openTupleCount = 0; i < snailNumString.length; i++) {
    if (snailNumString[i] === "[") {
      openTupleCount++;
      if (openTupleCount === 5) {
        const tupleString = snailNumString.slice(
          i,
          snailNumString.slice(i).indexOf("]") + i + 1
        );
        const tuple = JSON.parse(tupleString);

        const rightMatches = Array.from(
          snailNumString.slice(i + tupleString.length - 1).matchAll(NUM_RE)
        );
        if (rightMatches.length) {
          const rightMatch = rightMatches[0];
          const rightIndices = rightMatch.indices[0];

          snailNumString =
            snailNumString.slice(
              0,
              rightIndices[0] + i + tupleString.length - 1
            ) +
            Number(tuple[1] + parseInt(rightMatch)) +
            snailNumString.slice(rightIndices[1] + i + tupleString.length - 1);
        }

        snailNumString =
          snailNumString.slice(0, i) +
          "0" +
          snailNumString.slice(i + tupleString.length); // snailNumString.replace(tupleString, "0");
        const leftMatches = Array.from(
          snailNumString.slice(0, i - 1).matchAll(NUM_RE)
        );
        if (leftMatches.length) {
          const leftMatch = leftMatches[leftMatches.length - 1];
          const leftIndices = leftMatch.indices[0];
          snailNumString =
            snailNumString.slice(0, leftIndices[0]) +
            Number(tuple[0] + parseInt(leftMatch)) +
            snailNumString.slice(leftIndices[1]);
        }

        return [JSON.parse(snailNumString), true];
      }
    } else if (snailNumString[i] === "]") {
      openTupleCount--;
    }
  }
  return [snailNum, false];
};

// const explode = (
//   snailNum: SnailfishNumber[],
//   depth: number,
//   leftVal: number,
//   rightVal: number,
//   hasExploded: boolean
// ): [SnailfishNumber, number, number, boolean] => {
//   let leftVal, rightVal, _;
//   if (
//     depth === 4 &&
//     typeof snailNum[0] === "number" &&
//     typeof snailNum[1] === "number" &&
//     !hasExploded
//   ) {
//     return [0, snailNum[0], snailNum[1], true];
//   }
//   if (Array.isArray(snailNum[0])) {
//     [snailNum[0], leftVal, rightVal, hasExploded] = explode(
//       snailNum[0],
//       depth + 1,
//       leftVal,
//       rightVal,
//       hasExploded
//     );
//     if (typeof snailNum[1] === "number") {
//       snailNum[1] += rightVal;
//       rightVal = 0;
//     }
//   } else {
//     snailNum[0] += leftVal; // + rightVal;
//     leftVal = 0;
//   }

//   if (Array.isArray(snailNum[1])) {
//     [snailNum[1], leftVal, rightVal, hasExploded] = explode(
//       snailNum[1],
//       depth + 1,
//       rightVal, // rightVal only propogates up in leftVals position
//       0, // rightVal position never propogates up
//       hasExploded
//     );
//     if (leftVal && Array.isArray(snailNum[0])) {
//       // propogate leftVal back up
//       [snailNum[0], _, _, hasExploded] = explode(
//         snailNum[0],
//         depth + 1,
//         0,
//         leftVal,
//         true
//       );
//     }
//     if (typeof snailNum[0] === "number") {
//       snailNum[0] += leftVal;
//       leftVal = 0;
//     }
//   } else {
//     snailNum[1] += rightVal;
//     rightVal = 0;
//   }
//   return [[snailNum[0], snailNum[1]], leftVal, rightVal, hasExploded];
// };

const split = (
  snailNum: SnailfishNumber,
  hasSplit: boolean
): [SnailfishNumber, boolean] => {
  if (hasSplit) {
    return [snailNum, hasSplit];
  }
  if (Array.isArray(snailNum[0])) {
    [snailNum[0], hasSplit] = split(snailNum[0], hasSplit);
  } else if (snailNum[0] >= 10) {
    return [
      [[Math.floor(snailNum[0] / 2), Math.ceil(snailNum[0] / 2)], snailNum[1]],
      true,
    ];
  }
  if (hasSplit) {
    return [snailNum, hasSplit];
  }
  if (Array.isArray(snailNum[1])) {
    [snailNum[1], hasSplit] = split(snailNum[1], hasSplit);
  } else if (snailNum[1] >= 10) {
    return [
      [snailNum[0], [Math.floor(snailNum[1] / 2), Math.ceil(snailNum[1] / 2)]],
      true,
    ];
  }
  return [snailNum, hasSplit];
};

const add = (
  snailNumLeft: SnailfishNumber,
  snailNumRight: SnailfishNumber
): SnailfishNumber => reduce([snailNumLeft, snailNumRight]);

const addArray = (snailNums: SnailfishNumber[]) =>
  snailNums
    .slice(1)
    .reduce((acc, currentSnailNum) => add(acc, currentSnailNum), snailNums[0]);

const reduce = (snailNum: SnailfishNumber): SnailfishNumber => {
  let hasChanged, _;
  do {
    [snailNum, hasChanged] = explodeAsString(snailNum);
    if (!hasChanged) {
      [snailNum, hasChanged] = split(snailNum, false);
    }
  } while (hasChanged);
  return snailNum;
};

const magnitude = (snailNum: SnailfishNumber): number => {
  const left = Array.isArray(snailNum[0])
    ? magnitude(snailNum[0])
    : snailNum[0];
  const right = Array.isArray(snailNum[1])
    ? magnitude(snailNum[1])
    : snailNum[1];

  return 3 * left + 2 * right;
};

const runTests = () => {
  console.log(magnitude(JSON.parse("[[1,2],[[3,4],5]]")) === 143);
  console.log(
    magnitude(JSON.parse("[[[[0,7],4],[[7,8],[6,0]]],[8,1]]")) === 1384
  );
  console.log(magnitude(JSON.parse("[[[[1,1],[2,2]],[3,3]],[4,4]]")) === 445);
  console.log(magnitude(JSON.parse("[[[[3,0],[5,3]],[4,4]],[5,5]]")) === 791);
  console.log(magnitude(JSON.parse("[[[[5,0],[7,4]],[5,5]],[6,6]]")) === 1137);
  console.log(
    magnitude(
      JSON.parse("[[[[8,7],[7,7]],[[8,6],[7,7]]],[[[0,7],[6,6]],[8,7]]]")
    ) === 3488
  );

  console.log(
    JSON.stringify(
      explodeAsString(parseInput("[[[[[9,8],1],2],3],4]")[0])[0]
    ) === "[[[[0,9],2],3],4]"
  );
  console.log(
    JSON.stringify(
      explodeAsString(parseInput("[7,[6,[5,[4,[3,2]]]]]")[0])[0]
    ) === "[7,[6,[5,[7,0]]]]"
  );
  console.log(
    JSON.stringify(
      explodeAsString(parseInput("[[6,[5,[4,[3,2]]]],1]")[0])[0]
    ) === "[[6,[5,[7,0]]],3]"
  );

  console.log(
    JSON.stringify(
      explodeAsString(parseInput("[[3,[2,[1,[7,3]]]],[6,[5,[4,[3,2]]]]]")[0])[0]
    ) === "[[3,[2,[8,0]]],[9,[5,[4,[3,2]]]]]"
  );

  console.log(
    JSON.stringify(
      explodeAsString(parseInput("[[3,[2,[8,0]]],[9,[5,[4,[3,2]]]]]")[0])[0]
    ) === "[[3,[2,[8,0]]],[9,[5,[7,0]]]]"
  );

  console.log(
    JSON.stringify(
      split(parseInput("[[[[0,7],4],[15,[0,13]]],[1,1]]")[0], false)
    )
  );

  console.log(
    JSON.stringify(
      reduce(
        add(
          parseInput("[[[[4,3],4],4],[7,[[8,4],9]]]\n[1,1]")[0],
          parseInput("[[[[4,3],4],4],[7,[[8,4],9]]]\n[1,1]")[1]
        )
      )
    )
  );
};
const partOne = (input: SnailfishNumber[]): number =>
  magnitude(addArray(input));

const partTwo = (input: SnailfishNumber[]): number => {
  let maxMagnitude = -1;
  for (let i = 0; i < input.length; i++) {
    for (let j = 0; j < input.length; j++) {
      if (i === j) {
        continue;
      }
      const currentMagnitude = magnitude(addArray([input[i], input[j]]));
      if (currentMagnitude > maxMagnitude) {
        maxMagnitude = currentMagnitude;
      }
    }
  }
  return maxMagnitude;
};

//runTests();
console.log(`Part one: ${partOne(parseInput(getInput()))}`); // 3551
console.log(`Part two: ${partTwo(parseInput(getInput()))}`); // 4555
