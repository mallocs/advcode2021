import getInput from '../getInput'

const parseInput = (input : string) => input.split("\n").map(Number)

const partOne = (input : Array<number>) : number => {
    let lastNum = Infinity;
    return input.reduce((previous, current) => {
        if (current > lastNum) {
            previous++
        }
        lastNum = current
        return previous
    }, 0)
}

const partTwo = (input : Array<number>) : number => {
    const cache = input.slice(0, 3)
    let lastSum = cache.reduce((partialSum, current) => partialSum + current, 0)

    return [...input.slice(3)].reduce((previous, current) => {
        let sum = lastSum - (cache.shift() || 0) + current
        cache.push(current)
        if (sum > lastSum) {
            previous++
        }
        lastSum = sum
        return previous
    }, 0)
}

const inputArray = parseInput(getInput())
console.log(`Part one: ${partOne(inputArray)}`)
console.log(`Part two: ${partTwo(inputArray)}`)